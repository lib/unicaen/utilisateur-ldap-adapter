<?php

namespace UnicaenUtilisateurLdapAdapter\Service;

use UnicaenLdap\Entity\People as PeopleEntity;
use UnicaenLdap\Exception;
use UnicaenLdap\Filter\People as PeopleFilter;
use UnicaenLdap\Service\LdapPeopleServiceAwareTrait;
use UnicaenLdap\Service\People as PeopleService;
use UnicaenUtilisateur\Exception\RuntimeException;
use UnicaenUtilisateur\Service\RechercheIndividu\RechercheIndividuResultatInterface;
use UnicaenUtilisateur\Service\RechercheIndividu\RechercheIndividuServiceInterface;
use UnicaenUtilisateurLdapAdapter\Entity\LdapIndividu;

class LdapService implements RechercheIndividuServiceInterface
{
    use LdapPeopleServiceAwareTrait;

    /**
     * @param $id
     * @return RechercheIndividuResultatInterface
     */
    public function findById($id)
    {
        $filter = PeopleFilter::uid($id);
        /**
         * @var PeopleEntity $people
         */

        /** TODO retirer ce contournement une fois le problème compris */
//        $people = $this->ldapPeopleService->get($id);
        $people = $this->ldapPeopleService->searchAttributes($filter, ['id', 'uid', 'supannaliaslogin', 'supannAliasLogin','displayName', 'mail']);
        $peep = $people[$id];
        $p = new LdapIndividu();
        $p->setPeople($peep);
        return $p;
    }

    /**
     * @param string $term
     * @return RechercheIndividuResultatInterface[]
     */
    public function findByTerm(string $term)
    {
        $people = null;
        $filter = PeopleFilter::orFilter(
            PeopleFilter::username($term),
            PeopleFilter::nameContains($term)
        );
        /** @var PeopleService $ldapService */
        try {
            $people = $this->ldapPeopleService->searchAttributes($filter, ['id', 'uid', 'supannaliaslogin', 'supannAliasLogin','displayName', 'mail']);
//            $people = $this->ldapPeopleService->search($filter);
        } catch (Exception $e) {
            throw new RuntimeException("Un exception ldap est survenue :", $e);
        }

        $res = [];
        /** @var PeopleEntity $peep */
        foreach ($people as $peep) {
            $p = new LdapIndividu();
            $p->setPeople($peep);
            $res[] = $p;
        }
        return $res;
    }

    public function findByUsername(string $username)
    {
        $people = null;
        $filter = PeopleFilter::username($username);
        /** @var PeopleService $ldapService */
        try {
            $people = $this->ldapPeopleService->search($filter);
        } catch (Exception $e) {
            throw new RuntimeException("Une exception ldap est survenue :", $e);
        }

        if(sizeof($people) > 1):
            throw new RuntimeException("Plusieurs personnes correspondent au username.");
        endif;


        $res = null;
        /** @var PeopleEntity $peep */
        foreach ($people as $peep) {
            $p = new LdapIndividu();
            $p->setPeople($peep);
            $res = $p;
        }
        return $res;
    }
}